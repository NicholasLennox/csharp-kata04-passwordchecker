# Kata: Password strength checker

[![standard-readme compliant](https://img.shields.io/badge/standard--readme-OK-green.svg?style=flat-square)](https://github.com/RichardLitt/standard-readme)

Create a method that takes in a string password and checked its strength. The strength is determined as either "strong", "moderate", or "weak". The strength is determined by a score that increased based on the length, presence of uppercase and lowercase letters, the presence of a number, and the use of special characters. The application is tested using XUnit and run in Visual Studio. 

## Table of Contents

- [Install](#install)
- [Usage](#usage)
- [Maintainers](#maintainers)
- [Contributing](#contributing)
- [License](#license)

## Install

- Clone 
- Open in Visual Studio
- Build the application

## Usage
- Run the tests from Visual Studio

## Maintainers

[Nicholas Lennox](https://github.com/NicholasLennox)

## Contributing

Small note: If editing the README, please conform to the [standard-readme](https://github.com/RichardLitt/standard-readme) specification.

## License

MIT © 2021 Noroff Accelerate AS
