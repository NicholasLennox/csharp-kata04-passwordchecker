﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Kata4_PasswordChecker.Util
{
    public class PasswordChecker
    {
        public string CheckStrength(string password)
        {
            if(password.Length < 6)
                return "Invalid";
            if (password.Split(' ').Length > 1)
                return "Invalid";

            // Variable to hold the password strength
            int strength = 0;

            // Use Linq to check for uppercase, lowercase, digits
            bool hasGoodLength = password.Length >= 8;
            bool hasLowercase = password.Any(char.IsLower);
            bool hasUppercase = password.Any(char.IsUpper);
            bool hasNumber = password.Any(char.IsDigit);
            bool hasSpecial = password.Any(c => !char.IsLetterOrDigit(c));

            // Using turnary operation to add to strength
            strength += hasGoodLength ? 1 : 0;
            strength += hasLowercase ? 1 : 0;
            strength += hasUppercase ? 1 : 0;
            strength += hasNumber ? 1 : 0;
            strength += hasSpecial ? 1 : 0;

            // Based on our strength score, we need to return strong, moderate, or weak
            if (strength == 5)
                return "Strong";
            if (strength >= 3)
                return "Moderate";
            return "Weak";
        }
    }
}
